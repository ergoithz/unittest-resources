Welcome to unittest_resources's documentation!
==============================================

Dynamic resource-based unittest suites made easy.

Usage
=====

See `README.md <https://gitlab.com/ergoithz/unittest-resources/blob/master/README.md>`_.

Contents
========

.. toctree::
   :maxdepth: 2

   unittest_resources
   examples
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

License
=======

.. include:: ../LICENSE
